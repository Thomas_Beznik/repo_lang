.. _part2:

*************************************************************************************************
Partie 2 | Parsing
*************************************************************************************************

CFG Disambiguation
========================================================== proposed by Group 14, Beznik Thomas and Delaunoy Valentin

Given the following grammar:

* S -> AA
* A -> SS | a

1. Show that this grammar is ambiguous
""""""""""""""""""""""""""""""""""""""

2. Explain the language described by the grammar
""""""""""""""""""""""""""""""""""""""""""""""""

3. Disambiguate the grammar
"""""""""""""""""""""""""""

Answers
"""""""

1.
""

Diagram 1:
	.. image:: img/14-diagram1.png

2.
""
	.. math::
		a^(2+3n)
3.
""

* S -> aaA
* A -> aaaA | epsilon